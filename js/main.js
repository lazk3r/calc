const display = document.getElementById('display');
const btnsNumbers = document.getElementsByName('number');
const btnDelete = document.getElementById('delete');
const btnReset = document.getElementById('reset');
const btnEqual = document.getElementById('equal');
const btnsOperation = document.getElementsByName('operator')

window.addEventListener('resize', () => {
  let vh = window.innerHeight * 0.01;
  document.documentElement.style.setProperty('--vh', `${vh}px`);
});

btnsNumbers.forEach(btn => {
  btn.onclick = () => {
    display.innerHTML += `${btn.innerHTML}`;
  };
});

btnDelete.onclick = () => {
  const old = display.innerHTML;
  display.innerHTML = '';
  for (let i = 0; i < old.length - 1; i++) {
    display.innerHTML += `${old[i]}`;
  }
};

btnReset.onclick = () => {
  display.innerHTML = '';
}

btnsOperation.forEach(btn => {
  btn.onclick = () => {
    if (btn.innerHTML === 'x') {
      display.innerHTML += '*';
    } else {
      display.innerHTML += `${btn.innerHTML}`;
    }
  };
});

btnEqual.onclick = () => {
  display.innerHTML = `${eval(display.innerHTML)}`;
}
