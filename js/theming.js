const theme1 = document.getElementById('theme1');
const theme2 = document.getElementById('theme2');
const theme3 = document.getElementById('theme3');

let cookies = document.cookie
  .split(';')
  .map(cookie => cookie.split('='))
  .reduce((accumulator, [key, value]) =>
    ({ ...accumulator, [key.trim()]: decodeURIComponent(value) }),
    {});

if (!cookies.theme) {
  theme1.checked = true;
  document.cookie = 'theme=defalut';
} else {
  if (cookies.theme === 'defalut') {
    theme1.checked = true;
  } else if (cookies.theme === 'light') {
    theme2.checked = true;
    document.body.classList.add('lightTheme');
  } else {
    theme3.checked = true;
    document.body.classList.add('tokyoNightTheme');
  }
}

theme1.addEventListener('change', () => {
  if (theme1.checked) {
    document.body.classList.remove('lightTheme', 'tokyoNightTheme');
    document.cookie = 'theme=defalut';
  }
});

theme2.addEventListener('change', () => {
  if (theme2.checked) {
    document.body.classList.remove('defaultTheme', 'tokyoNightTheme');
    document.body.classList.add('lightTheme');
    document.cookie = 'theme=light';
  }
});

theme3.addEventListener('change', () => {
  if (theme3.checked) {
    document.body.classList.remove('defaultTheme', 'lightTheme');
    document.body.classList.add('tokyoNightTheme');
    document.cookie = 'theme=tokyo';
  }
});
